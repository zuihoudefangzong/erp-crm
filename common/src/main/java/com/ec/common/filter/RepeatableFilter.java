package com.ec.common.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import com.ec.common.utils.StringUtils;

/**
 * RepeatableFilter类实现了Filter接口
 * 这表明该类是一个过滤器，用于处理 Servlet 请求和响应
 * @author ec
 */
public class RepeatableFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // init 方法在过滤器初始化时被调用。在这个方法中，你可以进行过滤器的初始化工作，例如读取配置或设置资源
    }


    /**
     * doFilter 方法是过滤器的核心方法
     * 它接收三个参数：ServletRequest、ServletResponse 和 FilterChain
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        ServletRequest requestWrapper = null;
        // 检查 request 是否为 HttpServletRequest 类型
        if (request instanceof HttpServletRequest
                && StringUtils.startsWithIgnoreCase(request.getContentType(), MediaType.APPLICATION_JSON_VALUE)) {
            // 来确定是否创建一个新的 RepeatedlyRequestWrapper 对象并将其赋值给 requestWrapper
            // 构建可重复读取inputStream的request
            requestWrapper = new RepeatedlyRequestWrapper((HttpServletRequest) request, response);
        }
        if (null == requestWrapper) {
            // 如果 requestWrapper 为 null，
            // 则直接通过 chain.doFilter(request, response) 调用过滤器链中的下一个过滤器或目标 Servlet
            chain.doFilter(request, response);
        } else {
            // requestWrapper不为null
            // 调用 chain.doFilter(requestWrapper, response)
            chain.doFilter(requestWrapper, response);
        }
    }

    @Override
    public void destroy() {

    }
}
