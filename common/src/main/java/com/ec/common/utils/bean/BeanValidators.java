package com.ec.common.utils.bean;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

/**
 * bean对象属性验证 静态方法  私有是私有 静态是静态->class.方法名
 * 段代码的目的是使用提供的 Validator 对象对给定的对象进行属性验证。
 * 如果验证过程中发现了任何约束违规，就会抛出一个 ConstraintViolationException 异常，
 * 其中包含了所有的违规信息。这样可以在代码的其他部分捕获并处理这个异常
 * @author ec
 */
public class BeanValidators {
    public static void validateWithException(Validator validator, Object object, Class<?>... groups)
            throws ConstraintViolationException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        // 校验信息不为空
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
    }
}
