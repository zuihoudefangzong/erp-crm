package com.ec.common.enums;

/**
 * 数据源
 * 主库 分库 主从复制 读写分离
 * @author ec
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
