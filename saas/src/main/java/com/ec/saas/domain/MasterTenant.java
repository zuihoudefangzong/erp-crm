package com.ec.saas.domain;

import com.ec.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 实体类
 * @Author: xxxx
 * @Date: 2021/7/25
 * @Description:
 */
@Data
public class MasterTenant extends BaseEntity {
    // 主键自增id
    private Long id;
    // 估计是租户id 虽然存的名字tenant1
    // 后期优化可以用UUID
    private String tenant;
    private String url;
    // 库隔离 库名字
    private String databaseName;
    // 这里的username和password 是用户体系的管理员账号和密码
    // 数据库密码是在配置文件里面的
    private String username;
    private String password;
    private String hostName;
    private String status;
    private Date expirationDate;
}
