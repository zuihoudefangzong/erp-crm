package com.ec.saas.mapper;

import com.ec.saas.domain.MasterTenant;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 租户
 *
 * @author ec
 */
@Mapper
@Repository
public interface MasterTenantMapper {

    /**
     * 查询指定租户信息
     * @param masterTenant
     * @return
     */
    MasterTenant selectMasterTenant(MasterTenant masterTenant);

    /**
     * 根据租户id查询租户是否存在
     * @param tenantName
     * @return
     */
    int checkTenantNameUnique(String tenantName);

    /**
     * 新增租户
     *
     * @param masterTenant 租户数据库连接信息
     * @return 结果
     */
    int insertMasterTenant(MasterTenant masterTenant);

    /**
     * 查询到租户列表
     * @param masterTenant
     * @return
     */
    List<MasterTenant> selectMasterTenantList(MasterTenant masterTenant);

    /**
     * 根据主键id查到组户的详细信息
     * @param id
     * @return
     */
    MasterTenant selectMasterTenantById(Long id);


    /**
     * 更新数据
     * @param tenant
     * @return
     */
    int updateMasterTenant(MasterTenant tenant);


    /**
     * 根据主键id删除租户信息
     * 后期应该是软删除的
     * @param ids
     * @return
     */
    public int deleteMasterTenantByIds(Long[] ids);
}
