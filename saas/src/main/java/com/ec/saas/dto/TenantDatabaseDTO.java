package com.ec.saas.dto;

import lombok.Data;

/**
 * 注册的时候配置数据库相关信息
 * 后期不能更改了 涉及到迁移某个库(库隔离)
 * 慕课网运维系列的DBA数据库工程师
 * @Author:
 * @Date:
 * @Description:
 */
@Data
public class TenantDatabaseDTO {
    public String tenantDatabase;
    public String dbUser;
    public String dbPass;
    public String adminName;
    public String adminPass;
    // 租户名字
    public String tenantName;
    public String url;
}
