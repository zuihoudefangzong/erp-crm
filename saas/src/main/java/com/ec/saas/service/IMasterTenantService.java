package com.ec.saas.service;

import com.ec.saas.domain.MasterTenant;
import com.ec.saas.dto.TenantDatabaseDTO;

import java.util.List;

public interface IMasterTenantService {

    /**
     * 根据租户id查询租户信息
     * @param tenant
     * @return
     */
    MasterTenant selectMasterTenant(String tenant);


    /**
     * 根据租户id查询租户是否存在
     * @param tenantName
     * @return
     */
    String checkTenantNameUnique(String tenantName);


    /**
     * 新增租户
     *
     * @param tenantDatabaseDTO 租户数据库连接信息
     * @return 结果
     */
    int insertMasterTenant(TenantDatabaseDTO tenantDatabaseDTO);


    /**
     * 查询租户列表 有点看不懂
     * @param masterTenant
     * @return
     */
    List<MasterTenant> selectMasterTenants(MasterTenant masterTenant);


    /**
     * 根据主键id查到组户的详细信息
     * @param id
     * @return
     */
    public MasterTenant selectMasterTenantById(Long id);


    /**
     * 修改保存租户信息
     *
     * @param tenant 租户信息
     * @return 结果
     */
    public int updateMasterTenant(MasterTenant tenant);


    /**
     * 删除租户
     * @param ids
     * @return
     */
    public int deleteMasterTenantByIds(Long[] ids);

}
