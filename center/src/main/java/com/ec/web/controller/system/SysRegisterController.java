package com.ec.web.controller.system;

import com.ec.common.constant.TenantConstants;
import com.ec.auth.web.service.SysLoginService;
import com.ec.saas.service.IMasterTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ec.common.core.controller.BaseController;
import com.ec.common.core.domain.AjaxResult;
import com.ec.auth.web.service.TenantRegisterService;

import com.ec.saas.form.TenantRegisterBody;
import com.ec.saas.dto.TenantDatabaseDTO;
import java.sql.SQLException;

/**
 * 注册验证
 *
 * @author ec
 */
@RestController
public class SysRegisterController extends BaseController {


    @Autowired
    private IMasterTenantService masterTenantService;

    @Autowired
    private SysLoginService loginService;

    @Autowired
    private TenantRegisterService tenantRegisterService;



    @PostMapping("/register")
    public AjaxResult registerTenant(@RequestBody TenantRegisterBody tenantRegisterBody) {
        loginService.validateCaptcha(tenantRegisterBody.getTenantName(), tenantRegisterBody.getCode(), tenantRegisterBody.getUuid());

        if (TenantConstants.NOT_UNIQUE.equals(masterTenantService.checkTenantNameUnique(tenantRegisterBody.tenantName))) {
            return AjaxResult.error("注册'" + tenantRegisterBody.getTenantName() + "'失败，账号已存在");
        }
        TenantDatabaseDTO tenantDatabase = null;
        try {
            // 初始化租户的建库和建表 存原生的JDBC操作
            // 这时候没有执行动态连接数据源
            tenantDatabase = tenantRegisterService.initDatabase(tenantRegisterBody);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return AjaxResult.error("注册'" + tenantRegisterBody.getTenantName() + "'失败，创建租户时发生错误");
        } catch (Exception ex) {
            ex.printStackTrace();
            return AjaxResult.error("注册'" + tenantRegisterBody.getTenantName() + "'失败，请与我们联系");
        }

        // 新租户数据信息
        // 租户管理的数据库 插入新租户
        int i = masterTenantService.insertMasterTenant(tenantDatabase);
        return toAjax(i);
    }
}
