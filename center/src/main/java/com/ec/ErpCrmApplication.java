package com.ec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 主程序
 * 暂时无法启动 log配置不全
 * 扫描所有base 基础包 排除数据源的自动连接
 * @author ec
 */
@SpringBootApplication(scanBasePackages = "com", exclude = {DataSourceAutoConfiguration.class})
public class ErpCrmApplication {
    public static void main(String[] args) {
        SpringApplication.run(ErpCrmApplication.class, args);
        System.out.println("ErpCrm启动成功=======");
    }
}
