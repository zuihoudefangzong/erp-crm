package com.ec.auth.config;

import com.ec.auth.datasource.DynamicRoutingDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;


/**
 * 默认数据源
 * 分库是在mysql中指动态创建数据库database，独立数据库安全级别，不是按字段隔离。
 */
@Configuration
@Slf4j
public class DataSourceConfigurer {

    @Value("${spring.datasource.druid.master.url}")
    private String url;

    @Value("${spring.datasource.druid.master.username}")
    private String username;

    @Value("${spring.datasource.druid.master.password}")
    private String password;

    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;


    /**
     * 默认数据源的内容 这里也可以进行加密解密password
     * @return
     */
    private Map<String, Object> getProperties() {
        Map<String, Object> map = new HashMap<>();
        map.put("driverClassName", driverClassName);
        map.put("url", url);
        map.put("username", username);
        map.put("password", password);
        return map;
    }


    /**
     * 注册bean dynamicDataSource
     * @return
     */
    @Bean("dynamicDataSource")
    public DynamicRoutingDataSource dynamicDataSource() {
        DynamicRoutingDataSource dynamicRoutingDataSource = new DynamicRoutingDataSource();
        Map<Object, Object> dataSourceMap = new HashMap<>(1);
        // 默认数据源的名字 和 对应的关系
        dataSourceMap.put("default_db", dynamicRoutingDataSource.dataSource(getProperties()));
        // class DynamicRoutingDataSource extends AbstractRoutingDataSource
        dynamicRoutingDataSource.setTargetDataSources(dataSourceMap);

        // 设置默认数据源
        dynamicRoutingDataSource.setDefaultTargetDataSource(dynamicRoutingDataSource.dataSource(getProperties()));
        return dynamicRoutingDataSource;
    }
}
