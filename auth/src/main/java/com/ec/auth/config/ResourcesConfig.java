package com.ec.auth.config;

import com.ec.auth.interceptor.TenantInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.ec.common.config.ErpCrmConfig;
import com.ec.common.constant.Constants;
import com.ec.auth.interceptor.RepeatSubmitInterceptor;

/**
 * 通用配置
 * 实现了 WebMvcConfigurer 接口，用于配置WebMvc相关的设置
 * @author ec
 */
@Configuration// 表明该类是一个配置类，用于提供 Spring 框架的配置
public class ResourcesConfig implements WebMvcConfigurer {

    /**
     * 注入了 RepeatSubmitInterceptor 和 TenantInterceptor 实例，
     * 这两个实例可能用于处理请求拦截和资源处理
     */
    @Autowired
    private RepeatSubmitInterceptor repeatSubmitInterceptor;

    @Autowired
    private TenantInterceptor tenantInterceptor;

    /**
     * 用于添加资源处理器
     * 它指定了本地文件上传的路径和 Swagger 的资源位置
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        /** 本地文件上传路径 */
        registry.addResourceHandler(Constants.RESOURCE_PREFIX + "/**")
                .addResourceLocations("file:" + ErpCrmConfig.getProfile() + "/");

        /** swagger配置 */
        registry.addResourceHandler("/swagger-ui/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/");
    }

    /**
     * 自定义拦截规则
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // repeatSubmitInterceptor 添加为拦截器，并应用于所有路径。
        registry.addInterceptor(repeatSubmitInterceptor).addPathPatterns("/**");
        // tenantInterceptor 也被添加为拦截器，但排除了 /captchaImage 和 /register 路径
        registry.addInterceptor(tenantInterceptor).addPathPatterns("/**").excludePathPatterns("/captchaImage").excludePathPatterns("/register");
    }

    /**
     * 跨域配置 Security安全config配置用到  @Autowired自动注入
     * 这是一个自定义的 Bean 方法，用于创建跨域资源共享（CORS）过滤器
     * 全局开启跨域  以便前端调用接口
     */
    @Bean
    public CorsFilter corsFilter() {
        // 通过设置 CorsConfiguration 对象，允许跨域请求，
        // 并设置了一些配置项，如允许凭证、允许的源地址、请求头和方法
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // 设置访问源地址
        config.addAllowedOriginPattern("*");
        // 设置访问源请求头
        config.addAllowedHeader("*");
        // 设置访问源请求方法
        config.addAllowedMethod("*");
        // 有效期 1800秒
        config.setMaxAge(1800L);
        // 添加映射路径，拦截一切请求
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        // 返回新的CorsFilter
        return new CorsFilter(source);
    }
}
