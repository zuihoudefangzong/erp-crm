package com.ec.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.web.filter.CorsFilter;
import com.ec.auth.security.filter.JwtAuthenticationTokenFilter;
import com.ec.auth.security.handle.AuthenticationEntryPointImpl;
import com.ec.auth.security.handle.LogoutSuccessHandlerImpl;


/**
 * 因此，一般来说，常见的安全管理技术栈的组合是这样的：
 * SSM + Shiro
 *  Spring Boot/Spring Cloud + Spring Security
 * 用户认证: 通俗点说就是系统认为用户是否能登录
 * 用户授权: 通俗点讲就是系统判断用户是否有权限去做某些事情
 * 首先用户认证 Authentication Manager校验所调用的三个组件我
 *  1.实现Security的接口 PasswordEncoder密码加密器
 *  2.自定义用户对象 继承User
 *  3.用户service业务对象 实现接口UserDetailsService
 *  然后再开始修改WebSecurityConfigurerAdapter配置类
 */
/**
 * spring security配置
 * SecurityConfig类继承自 WebSecurityConfigurerAdapter，用于配置 Spring Security 的安全设置
 * @author ec
 */
// 这个注解用于启用 Spring Security 的全局方法安全功能 它设置了前置和后置注解的启用状态 以及securedEnabled安全启用状态。
// Spring Security默认是禁用注解的，要想开启注解，
// 需要在继承WebSecurityConfigurerAdapter的类上加@EnableGlobalMethodSecurity注解，来判断用户对某个控制层的方法是否具有访问权限
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * 后期会自定义用户认证逻辑
     * 就是继承org.springframework.security.core.userdetails.User
     */
    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * 认证失败处理类 会调用的类实例
     * AuthenticationEntryPointImpl
     */
    @Autowired
    private AuthenticationEntryPointImpl unauthorizedHandler;

    /**
     * 退出处理类
     */
    @Autowired
    private LogoutSuccessHandlerImpl logoutSuccessHandler;

    /**
     * token认证过滤器
     */
    @Autowired
    private JwtAuthenticationTokenFilter authenticationTokenFilter;

    /**
     * 跨域过滤器
     */
    @Autowired
    private CorsFilter corsFilter;

    /**
     * 这个方法重写了父类的方法，用于创建 AuthenticationManager 对象。
     * 通过调用父类的方法来获取默认的认证管理器
     * 同时解决 无法直接注入 AuthenticationManager
     * 这里将Spring Security自带的authenticationManager声明成Bean，声明它的作用是用它帮我们进行认证操作，
     * 调用这个Bean的authenticate方法会由Spring Security自动帮我们做认证。
     * Authentication Manager校验
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * anyRequest          |   匹配所有请求路径
     * access              |   SpringEl表达式结果为true时可以访问
     * anonymous           |   匿名可以访问
     * denyAll             |   用户不能访问
     * fullyAuthenticated  |   用户完全认证可以访问（非remember-me下自动登录）
     * hasAnyAuthority     |   如果有参数，参数表示权限，则其中任何一个权限可以访问
     * hasAnyRole          |   如果有参数，参数表示角色，则其中任何一个角色可以访问
     * hasAuthority        |   如果有参数，参数表示权限，则其权限可以访问
     * hasIpAddress        |   如果有参数，参数表示IP地址，如果用户IP和参数匹配，则可以访问
     * hasRole             |   如果有参数，参数表示角色，则其角色可以访问
     * permitAll           |   用户可以任意访问
     * rememberMe          |   允许通过remember-me登录的用户访问
     * authenticated       |   用户登录后可访问
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                // CSRF禁用，因为不使用session
                .csrf().disable()
                // AuthenticationEntryPointImpl 类通常用于处理未经授权的请求，
                // 当用户尝试访问需要认证但尚未通过认证的资源时，该类会被调用
                // 认证失败处理类 AuthenticationEntryPointImpl的实例unauthorizedHandler
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                // 基于token，所以不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                // 过滤请求
                .authorizeRequests()
                // 对于登录login 注册register 验证码captchaImage 允许匿名访问
                .antMatchers("/login", "/register", "/captchaImage").anonymous()
                .antMatchers(
                        HttpMethod.GET,
                        "/",
                        "/*.html",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js",
                        "/profile/**"
                ).permitAll()
                // 还可放行资源路径
                // 放行图片、文件上传的资源路径
                .antMatchers("/swagger-ui.html").anonymous()
                .antMatchers("/swagger-resources/**").anonymous()
                .antMatchers("/webjars/**").anonymous()
                .antMatchers("/*/api-docs").anonymous()
                .antMatchers("/druid/**").anonymous()
                // 这里意思是其它所有接口需要认证才能访问 除上面外的所有请求全部需要鉴权认证
                .anyRequest().authenticated()
                .and()
                .headers().frameOptions().disable();// 防止iframe 造成跨域

        // 用户退出 调用的请求路径/logout
        httpSecurity.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);

        // 需要携带token的请求路径 一律要经过JwtAuthenticationTokenFilter的实例authenticationTokenFilter鉴权
        // 添加JWT filter 在UsernamePasswordAuthenticationFilter之前before
        // 这样做就是为了除了登录的时候去查询数据库外，其他时候都用token进行认证
        // 由于登录的方式很多 手机号码+验证 账号+密码 只做了是否携带token和租户字段认证
        // 没有继承UsernamePasswordAuthenticationFilter类 实现一个自动认证的子类
        httpSecurity.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class)
                  //.addFilter(TokenLoginFilter)
                ;// TokenLoginFilter可以继承spring-security的UsernamePasswordAuthenticationFilter实现自己的用户密码逻辑


        // JwtAuthenticationTokenFilter就是authenticationTokenFilter 一个实例化后的对象 一个类
        // 添加CORS filter 同时在JwtAuthenticationTokenFilter之前before
        httpSecurity.addFilterBefore(corsFilter, JwtAuthenticationTokenFilter.class);
        // 添加CORS filter 在LogoutFilter之前before
        httpSecurity.addFilterBefore(corsFilter, LogoutFilter.class);
        /**
         * 禁用 CSRF 防护。
         * 设置认证失败的处理方式，使用注入的 unauthorizedHandler。
         * 配置会话管理，采用无状态会话（不使用 session）。
         * 配置请求授权，指定哪些路径允许匿名访问，哪些路径需要认证。
         * 禁用框架选项。
         * 配置注销，设置注销的 URL 和注销成功的处理方式。
         * 添加 JWT 过滤器和 CORS 过滤器，并设置它们的顺序
         */
        // 禁用session 上面已经链式调用了
        // httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    /**
     * 强散列哈希加密实现
     * 这个方法创建了一个 BCryptPasswordEncoder 对象，用于密码的加密和解密
     * 一般后期会自定义密码加密器
     * 就是自定义类实现Security的接口 PasswordEncoder密码加密器
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 身份认证接口
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    public static void main(String[] args) {
        // 配置 BCryptPasswordEncoder
        BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

        // 未知的
        // $2a$10$mBRRF9VVzYkrVKYV8th94OehTQR0j93q7kwghIN2zDk8rlYeUT.MW
        // 要加密的明文密码
        String plainTextPassword = "admin";

        // 加密密码
        String encryptedPassword = bcryptEncoder.encode(plainTextPassword);

        // 输出加密后的密码
        System.out.println("Encrypted Password: " + encryptedPassword);
    }


    /**
     * 配置哪些请求不拦截
     * 排除swagger相关请求
     * @param web
     * @throws Exception
     */
/*    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/favicon.ico","/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**", "/doc.html");
    }*/
}
