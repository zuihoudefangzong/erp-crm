package com.ec.auth.config;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import com.ec.common.utils.ServletUtils;

/**
 * 服务相关配置
 *
 * @author ec
 */
@Component// 注册为spring的组件
public class ServerConfig {

    /**
     * 提取域名和上下文路径
     * 是一个静态方法，接收一个HttpServletRequest对象作为参数。
     * 它使用request.getRequestURL()获取请求的 URL，并通过一系列字符串操作来提取域名和上下文路径。
     * @param request
     * @return
     */
    public static String getDomain(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        String contextPath = request.getServletContext().getContextPath();
        return url.delete(url.length() - request.getRequestURI().length(), url.length()).append(contextPath).toString();
    }

    /**
     * 获取完整的请求路径，包括：域名，端口，上下文访问路径
     * ServletUtils会自动获取本次request
     * @return 服务地址
     */
    public String getUrl() {
        HttpServletRequest request = ServletUtils.getRequest();
        return getDomain(request);
    }
}
