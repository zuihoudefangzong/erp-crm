package com.ec.auth.security.handle;

import java.io.IOException;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSON;
import com.ec.common.constant.HttpStatus;
import com.ec.common.core.domain.AjaxResult;
import com.ec.common.utils.ServletUtils;
import com.ec.common.utils.StringUtils;

/**
 * 认证失败处理类 返回未授权
 * AuthenticationEntryPointImpl类实现了 AuthenticationEntryPoint 接口，用于处理认证失败的情况
 * AuthenticationEntryPoint接口interface就一个method
 * 还实现了 Serializable接口，以便可以序列化和反序列化该类的对象
 * @author ec
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable {
    private static final long serialVersionUID = -8970718410437077606L;

    /**
     * commence 方法是 AuthenticationEntryPoint 接口的方法，当认证失败时会被调用
     * @param request
     * @param response
     * @param e
     * @throws IOException
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException {
        // 首先设置了一个状态码 code 为 HttpStatus.UNAUTHORIZED，表示未授权
        int code = HttpStatus.UNAUTHORIZED;
        // 然后，通过 StringUtils.format 方法构建了一个错误消息 msg，其中包含了请求的 URI 和认证失败的信息
        String msg = StringUtils.format("请求访问：{}，认证失败，无法访问系统资源", request.getRequestURI());
        // 接下来，使用 ServletUtils.renderString 方法将错误信息作为响应体发送给客户端。
        // AjaxResult.error(code, msg) 用于创建一个包含错误状态码和错误消息的 AjaxResult 对象，并将其转换为 JSON 字符串
        ServletUtils.renderString(response, JSON.toJSONString(AjaxResult.error(code, msg)));
    }
}
