package com.ec.auth.security.custom;


import com.ec.common.core.domain.model.LoginUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * 自定义用户类
 * 给
 */
public class CustomUser extends User {

    /**
     * 我们自己的用户实体对象，要调取用户信息时直接获取这个实体对象
     */
    private LoginUser loginUser;

    public CustomUser(LoginUser loginUser, Collection<? extends GrantedAuthority> authorities) {
        // 给父类security User
        super(loginUser.getUsername(), loginUser.getPassword(), authorities);
        this.loginUser = loginUser;
    }

    public LoginUser getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(LoginUser loginUser) {
        this.loginUser = loginUser;
    }
}