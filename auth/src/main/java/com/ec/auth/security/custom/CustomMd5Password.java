package com.ec.auth.security.custom;



import com.ec.common.utils.sign.Md5Utils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * 自定义security 密码组件
 */
@Component
public class CustomMd5Password implements PasswordEncoder {

    /**
     * springframework.security把密码传过来
     * 我来加密
     * @param rawPassword
     * @return
     */
    @Override
    public String encode(CharSequence rawPassword) {
        return Md5Utils.hash(rawPassword.toString());
    }


    /**
     * 对比密码是否正确
     * @param rawPassword
     * @param encodedPassword
     * @return
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        // String encodedPassword应该是从数据库拿到的密码(假设存的是MD5后的)
        // 而rawPassword 是用户输入的密码
        return encodedPassword.equals(Md5Utils.hash(rawPassword.toString()));
    }
}
