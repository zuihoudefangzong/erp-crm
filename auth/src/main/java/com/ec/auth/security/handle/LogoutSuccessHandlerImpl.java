package com.ec.auth.security.handle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import com.alibaba.fastjson.JSON;
import com.ec.common.constant.Constants;
import com.ec.common.constant.HttpStatus;
import com.ec.common.core.domain.AjaxResult;
import com.ec.common.core.domain.model.LoginUser;
import com.ec.common.utils.ServletUtils;
import com.ec.common.utils.StringUtils;
import com.ec.auth.manager.AsyncManager;
import com.ec.auth.manager.factory.AsyncFactory;
import com.ec.auth.web.service.TokenService;

/**
 * 自定义退出处理类 返回成功
 * 实现LogoutSuccessHandler接口
 * @author ec
 */
@Configuration// @Configuration 注解用于将该类标记为 Spring 配置类
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {

    /**
     * tokenService是注册成spring的组件
     * 通过 @Autowired 注解，自动注入 TokenService 实例，以便在类中使用
     */
    @Autowired
    private TokenService tokenService;

    /**
     * 退出处理
     * onLogoutSuccess 方法是 LogoutSuccessHandler 接口的方法，在用户成功注销时被调用
     * 它接收 HttpServletRequest、HttpServletResponse 和 Authentication 对象作为参数
     * @return
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        // 通过调用 tokenService.getLoginUser(request) 获取登录用户的信息
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser)) {
            // 如果登录用户不为空，获取用户名并删除用户的缓存记录
            String userName = loginUser.getUsername();
            // 删除用户缓存记录
            tokenService.delLoginUser(loginUser.getToken());


            // 获取请求头中的租户信息
            String tenant = request.getHeader("tenant");

            // 使用 AsyncManager.me().execute 异步记录用户退出日志
            // 记录用户退出日志
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(tenant, userName, Constants.LOGOUT, "退出成功"));
        }
        // 使用 ServletUtils.renderString 方法将包含退出成功信息的 JSON 字符串作为响应发送给客户端
        ServletUtils.renderString(response, JSON.toJSONString(AjaxResult.error(HttpStatus.SUCCESS, "退出成功")));
    }
}
