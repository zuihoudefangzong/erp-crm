package com.ec.auth.interceptor.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import com.ec.common.annotation.RepeatSubmit;
import com.ec.common.constant.Constants;
import com.ec.common.core.redis.RedisCache;
import com.ec.common.filter.RepeatedlyRequestWrapper;
import com.ec.common.utils.StringUtils;
import com.ec.common.utils.http.HttpHelper;
import com.ec.auth.interceptor.RepeatSubmitInterceptor;

/**
 * 判断请求url和数据是否和上一次相同，
 * 如果和上次相同，则是重复提交表单。 有效时间为10秒内。
 * 具体的实现类SameUrlDataInterceptor，它继承自 RepeatSubmitInterceptor 抽象类
 * @author ec
 */

@Component// 该类是 Spring 组件，将由 Spring 容器进行管理
public class SameUrlDataInterceptor extends RepeatSubmitInterceptor {
    /**
     * SameUrlDataInterceptor 类扩展了 RepeatSubmitInterceptor，
     * 并定义了两个常量 REPEAT_PARAMS 和 REPEAT_TIME，用于表示重复提交的参数和时间。
     */
    // 请求的参数 有可能是查询参数/body的参数
    public final String REPEAT_PARAMS = "repeatParams";
    // 请求的时间
    public final String REPEAT_TIME = "repeatTime";

    // 令牌自定义标识
    @Value("${token.header}")
    private String header;

    /**
     * 它还注入了一个 RedisCache 对象，用于在 Redis 中进行缓存操作
     */
    @Autowired
    private RedisCache redisCache;

    /**
     * 这个方法是重写自父类的抽象方法，用于实现具体的防重复提交逻辑
     * @param request
     * @param annotation
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean isRepeatSubmit(HttpServletRequest request, RepeatSubmit annotation) {

        // 它首先获取当前请求的参数
        String nowParams = "";
        if (request instanceof RepeatedlyRequestWrapper) {
            RepeatedlyRequestWrapper repeatedlyRequest = (RepeatedlyRequestWrapper) request;
            // 获取http的body
            nowParams = HttpHelper.getBodyString(repeatedlyRequest);
        }

        // body参数为空，获取Parameter的数据
        if (StringUtils.isEmpty(nowParams)) {
            nowParams = JSONObject.toJSONString(request.getParameterMap());
        }
        // 并将其存储在 nowDataMap 中
        Map<String, Object> nowDataMap = new HashMap<String, Object>();
        nowDataMap.put(REPEAT_PARAMS, nowParams);
        nowDataMap.put(REPEAT_TIME, System.currentTimeMillis());

        // 请求地址（作为存放cache的key值）
        String url = request.getRequestURI();

        // 唯一值（没有消息头则使用请求地址）
        String submitKey = StringUtils.trimToEmpty(request.getHeader(header));

        // 唯一标识（指定key + url + 消息头）
        String cacheRepeatKey = Constants.REPEAT_SUBMIT_KEY + url + submitKey;

        // 获取redis的某个key
        Object sessionObj = redisCache.getCacheObject(cacheRepeatKey);
        if (sessionObj != null) {
            // 如果拿到
            Map<String, Object> sessionMap = (Map<String, Object>) sessionObj;
            if (sessionMap.containsKey(url)) {
                Map<String, Object> preDataMap = (Map<String, Object>) sessionMap.get(url);
                // 判断参数是否相同 判断两次间隔时间
                if (compareParams(nowDataMap, preDataMap) && compareTime(nowDataMap, preDataMap, annotation.interval())) {
                    return true;
                }
            }
        }
        // 不存在 就加到redis缓存中
        Map<String, Object> cacheMap = new HashMap<String, Object>();
        cacheMap.put(url, nowDataMap);
        redisCache.setCacheObject(cacheRepeatKey, cacheMap, annotation.interval(), TimeUnit.MILLISECONDS);
        return false;
    }

    /**
     * 判断参数是否相同
     */
    private boolean compareParams(Map<String, Object> nowMap, Map<String, Object> preMap) {
        String nowParams = (String) nowMap.get(REPEAT_PARAMS);
        String preParams = (String) preMap.get(REPEAT_PARAMS);
        return nowParams.equals(preParams);
    }

    /**
     * 判断两次间隔时间
     */
    private boolean compareTime(Map<String, Object> nowMap, Map<String, Object> preMap, int interval) {
        long time1 = (Long) nowMap.get(REPEAT_TIME);
        long time2 = (Long) preMap.get(REPEAT_TIME);
        if ((time1 - time2) < interval) {
            return true;
        }
        return false;
    }
}
