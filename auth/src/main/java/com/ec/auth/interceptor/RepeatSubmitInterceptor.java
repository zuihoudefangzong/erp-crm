package com.ec.auth.interceptor;

import java.lang.reflect.Method;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import com.alibaba.fastjson.JSONObject;
import com.ec.common.annotation.RepeatSubmit;
import com.ec.common.core.domain.AjaxResult;
import com.ec.common.utils.ServletUtils;

/**
 * 防止重复提交拦截器
 * 这是一个抽象类
 * @author ec
 */
/**
 * 这个接口定义了拦截器在 Spring Web 应用程序中的执行阶段，允许开发者在请求处理的不同阶段添加自己的逻辑和功能
 * 接口HandlerInterceptor定义了三个方法，用于在 Spring Web 应用程序的请求处理过程中的不同阶段进行拦截和操作
 * preHandle用于在请求处理之前进行预处理
 * postHandle 这也是一个默认方法，在请求处理之后但在视图渲染之前执行 不常用 现在架构都是前后端分离
 * afterCompletion方法 同样是一个默认方法，在请求处理完成后执行，无论是否发生异常
 */
@Component// 这表明该类是 Spring 组件，使得 Spring 容器可以自动管理该类的实例
public abstract class RepeatSubmitInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 它在 preHandle 方法中检查处理请求的对象是否为 HandlerMethod 类型
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            // 如果是，它获取对应的方法，并检查该方法上是否有 RepeatSubmit 注解
            RepeatSubmit annotation = method.getAnnotation(RepeatSubmit.class);

            // 如果有使用该注解
            if (annotation != null) {
                if (this.isRepeatSubmit(request, annotation)) {
                    AjaxResult ajaxResult = AjaxResult.error(annotation.message());
                    ServletUtils.renderString(response, JSONObject.toJSONString(ajaxResult));
                    return false;
                }
                // 如果this.isRepeatSubmit没有返回true
                // if判断体后面也会返回true
            }
            return true;
        } else {
            return true;
        }
    }

    /**
     * 验证是否重复提交由子类实现具体的防重复提交的规则
     * 有抽象方法的 必然是一个抽象类
     * 抽象方法在某个非抽象类(子类)下必须实现
     * @param request
     * @return
     * @throws Exception
     */
    public abstract boolean isRepeatSubmit(HttpServletRequest request, RepeatSubmit annotation);
}
