package com.ec.auth.aspectj;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;
import com.ec.common.annotation.RateLimiter;
import com.ec.common.enums.LimitType;
import com.ec.common.exception.ServiceException;
import com.ec.common.utils.ServletUtils;
import com.ec.common.utils.StringUtils;
import com.ec.common.utils.ip.IpUtils;

/**
 * 限流处理
 * 限流处理工具类
 * @author ec
 */

/**
 * @Aspect 注解表明这个类是一个切面（Aspect），用于定义横切关注点的代码，如日志记录、性能监控、事务管理等。
 * @Component 注解将该类标记为 Spring 管理的组件
 */
@Aspect
@Component
public class RateLimiterAspect {

    // log：这是一个静态 final 日志记录器，用于记录日志信息
    private static final Logger log = LoggerFactory.getLogger(RateLimiterAspect.class);

    // redisTemplate：这是一个 RedisTemplate 对象，用于与 Redis 数据库进行交互
    private RedisTemplate<Object, Object> redisTemplate;

    // limitScript：这是一个 RedisScript 对象，用于执行 Redis 中的限流脚本。
    private RedisScript<Long> limitScript;


    // @Autowired 注解用于自动注入依赖。
    // 通过这个注解，setRedisTemplate1 和 setLimitScript 方法会自动获取注入的 RedisTemplate 和 RedisScript 对象
    @Autowired
    public void setRedisTemplate1(RedisTemplate<Object, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Autowired
    public void setLimitScript(RedisScript<Long> limitScript) {
        this.limitScript = limitScript;
    }

    /**
     * @Before 注解用于定义一个前置通知（advice）。
     * 这个通知会在被 @annotation(rateLimiter) 注解标记的方法执行之前执行
     * @param point
     * @param rateLimiter
     * @throws Throwable
     */
    @Before("@annotation(rateLimiter)")
    public void doBefore(JoinPoint point, RateLimiter rateLimiter) throws Throwable {
        // 这个方法是前置通知的实现。它接受一个 JoinPoint 和一个 RateLimiter 参数
        String key = rateLimiter.key();
        int time = rateLimiter.time();
        int count = rateLimiter.count();

        String combineKey = getCombineKey(rateLimiter, point);
        List<Object> keys = Collections.singletonList(combineKey);
        // 然后，它通过调用 getCombineKey 方法获取组合键。

        try {
            // 接下来，它使用 RedisTemplate 执行限流脚本，并检查返回的数量是否超过限制。
            Long number = redisTemplate.execute(limitScript, keys, count, time);
            // 如果超过限制，它会抛出一个 ServiceException。
            if (StringUtils.isNull(number) || number.intValue() > count) {
                throw new ServiceException("访问过于频繁，请稍候再试");
            }
            log.info("限制请求'{}',当前请求'{}',缓存key'{}'", count, number.intValue(), key);
        } catch (ServiceException e) {
            // 如果发生其他异常，它会抛出一个运行时异常
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("服务器限流异常，请稍候再试");
        }finally {

        }
    }

    /**
     * 这个方法用于生成组合键。。
     * @param rateLimiter
     * @param point
     * @return
     */
    public String getCombineKey(RateLimiter rateLimiter, JoinPoint point) {
        // 它根据 RateLimiter 注解和 JoinPoint 的信息来构建键
        StringBuffer stringBuffer = new StringBuffer(rateLimiter.key());

        // 如果限流类型是 IP 限制，它会将 IP 地址添加到键中。然后，它将目标类名、方法名添加到键中
        if (rateLimiter.limitType() == LimitType.IP) {
            stringBuffer.append(IpUtils.getIpAddr(ServletUtils.getRequest())).append("-");
        }
        // 就是反射拿到
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        Class<?> targetClass = method.getDeclaringClass();
        stringBuffer.append(targetClass.getName()).append("-").append(method.getName());
        return stringBuffer.toString();
    }
}
