package com.ec.crm.form;

import lombok.Data;

/**
 * 线索提交 成为真正的客户了
 * @Author: xxxx
 * @Date: xxxx/1/14
 * @Description: 转化成客的DTO
 */
@Data
public class ToCustomerDTO {
    public Long id;
    public String name;
    public String customerStatus;
    public String customerRank;
}
